#include <iostream>
#include <vector>
#include <string>

std::string vectorCharToDecimalString(const std::vector<char>& binaryData) {
    std::string decimalString = "";
    for (char byte : binaryData) {
        int decimalValue = static_cast<int>(static_cast<unsigned char>(byte));
        decimalString += std::to_string(decimalValue);
    }
    return decimalString;
}

int main() {
    // std::vector<char> binaryData = { 
    //     char(0b11111100), 
    //     char(0b01010001), 
    //     char(0b11111110), 
    //     char(0b10011100), 
    //     char(0b01010000),
    //     char(0b01010000),
    //     char(0b01010000),
    //     char(0b01010000),
    //     char(0b01010000),
    //     char(0b01010000),
    //     char(0b01010000),
    //     char(0b01010000),
    //     char(0b01010000),
    //     char(0b01010000),
    //     char(0b01010000) 
    // };

    std::vector<char> binaryData = { 
        char(0b00000001), 
        char(0b01000100)
    };

    std::string decimalString = vectorCharToDecimalString(binaryData);

    std::cout << "Decimal string: " << decimalString << std::endl;

    return 0;
}
