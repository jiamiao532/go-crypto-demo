# Rust openssl BigNumber Implementation


## openssl/src/bn.rs


```Rust
#[proc_macro_attribute]
pub fn corresponds(attr: TokenStream, item: TokenStream) -> TokenStream {
    let function = parse_macro_input!(attr as Ident);
    let item = parse_macro_input!(item as ItemFn);

    let function = function.to_string();
    let line = format!(
        "This corresponds to [`{0}`](https://www.openssl.org/docs/manmaster/man3/{0}.html).",
        function
    );

    let attrs = item.attrs;
    let vis = item.vis;
    let sig = item.sig;
    let block = item.block;

    let out = quote! {
        #(#attrs)*
        #[doc = ""]
        #[doc = #line]
        #[doc(alias = #function)]
        #vis #sig #block
    };
    out.into()
}
```





这段 Rust 代码定义了一个属性宏 `corresponds`，该宏可以用于标记函数，并自动生成文档注释，用于指明函数与 OpenSSL 文档中的函数的对应关系。

具体的代码逻辑如下：

1. `#![allow(clippy::uninlined_format_args)]` 是一个编译器属性，用于告诉 Rust 编译器不要警告关于 `clippy` 代码检查工具中的 `uninlined_format_args` 检查项的警告。这是为了允许在代码中使用特定的格式化字符串操作，而不触发 Clippy 的警告。

2. 使用了 `proc_macro` 相关的库，这是 Rust 中用于编写自定义宏的库。

3. `#[proc_macro_attribute]` 标志了这是一个属性宏。

4. `corresponds` 宏接受两个参数：`attr` 和 `item`。`attr` 用于指定函数的名称，`item` 包含了标记宏的函数的源代码。

5. 宏首先解析 `attr` 和 `item` 参数，将函数的名称保存到 `function` 变量中，将函数的源代码保存到 `item` 变量中。

6. 接下来，宏使用函数的名称生成一个包含文档链接的字符串 `line`，这个链接指向 OpenSSL 文档中与该函数对应的文档页面。

7. 宏提取了函数的属性、可见性、签名和函数体，并将它们保存到相应的变量中。

8. 使用 `quote` 宏，宏将生成一段新的代码，其中包含了函数的属性、文档注释和函数体。在注释部分，使用 `#[doc = ""]` 来清除原始函数的文档注释，然后添加了一个新的文档注释，其中包含了与 OpenSSL 文档的链接。

9. 最后，将生成的代码包装为 `TokenStream` 并返回，以便在 Rust 程序中使用。

总之，这个属性宏 `corresponds` 用于自动生成函数的文档注释，以便指明函数与 OpenSSL 文档中的对应关系，提供更好的文档可读性和链接性。



`corresponds` 属性宏的目的是为 Rust 函数自动生成文档注释，以指明函数与 OpenSSL 文档中的函数的对应关系。这有助于提供更好的文档可读性和链接性，特别是在与外部库或工具的集成中。

下面是对 `corresponds` 属性宏的详细解释：

1. 属性宏定义：`corresponds` 属性宏是一个由 Rust 的 `proc_macro` 框架提供的自定义属性宏。属性宏通常用于为 Rust 代码添加自定义元数据或生成额外的代码。

2. 使用方式：`corresponds` 属性宏可以用于标记 Rust 函数。通过在函数上应用 `#[corresponds]` 属性，您可以启用这个宏来生成文档注释。

3. 参数：`corresponds` 宏接受两个参数：
   - `attr`：这是用于指定函数的名称的参数，它应该是一个标识符（Identifier），例如 `#[corresponds(some_function)]`。
   - `item`：这是包含了标记宏的函数的 Rust 源代码的参数。

4. 功能：
   - 解析参数：宏首先解析传递给宏的参数。它将函数的名称提取到 `function` 变量中，将函数的源代码提取到 `item` 变量中。
   - 生成文档注释：宏根据函数名称生成一个文档注释字符串 `line`，这个注释字符串包含一个链接，该链接指向 OpenSSL 文档中与该函数对应的文档页面。
   - 提取原始函数信息：宏提取了函数的属性、可见性、签名和函数体，以便在生成的代码中重新使用这些信息。
   - 生成新代码：宏使用 `quote` 宏生成一段新的 Rust 代码，其中包含了函数的属性、清空原始函数的文档注释、添加了一个新的文档注释（包含 OpenSSL 文档的链接），以及函数体。
   - 输出结果：最后，宏将生成的新代码包装为 `TokenStream` 并返回，以便在 Rust 程序中使用。

5. 效果：通过应用 `corresponds` 属性宏，您可以自动生成包含文档链接的注释，以明确指出 Rust 函数与 OpenSSL 文档中的函数的对应关系。这有助于使代码更易于理解，并提供了方便的文档导航功能。

总之，`corresponds` 属性宏是一个在 Rust 中自动生成文档链接注释的有用工具，它增强了代码文档的质量和可读性，特别是在与外部库或工具的集成中。



`corresponds` 属性宏的目的是为 Rust 函数自动生成文档注释，以指明函数与 OpenSSL 文档中的函数的对应关系。这有助于提供更好的文档可读性和链接性，特别是在与外部库或工具的集成中。

下面是对 `corresponds` 属性宏的详细解释：

1. 属性宏定义：`corresponds` 属性宏是一个由 Rust 的 `proc_macro` 框架提供的自定义属性宏。属性宏通常用于为 Rust 代码添加自定义元数据或生成额外的代码。

2. 使用方式：`corresponds` 属性宏可以用于标记 Rust 函数。通过在函数上应用 `#[corresponds]` 属性，您可以启用这个宏来生成文档注释。

3. 参数：`corresponds` 宏接受两个参数：
   - `attr`：这是用于指定函数的名称的参数，它应该是一个标识符（Identifier），例如 `#[corresponds(some_function)]`。
   - `item`：这是包含了标记宏的函数的 Rust 源代码的参数。

4. 功能：
   - 解析参数：宏首先解析传递给宏的参数。它将函数的名称提取到 `function` 变量中，将函数的源代码提取到 `item` 变量中。
   - 生成文档注释：宏根据函数名称生成一个文档注释字符串 `line`，这个注释字符串包含一个链接，该链接指向 OpenSSL 文档中与该函数对应的文档页面。
   - 提取原始函数信息：宏提取了函数的属性、可见性、签名和函数体，以便在生成的代码中重新使用这些信息。
   - 生成新代码：宏使用 `quote` 宏生成一段新的 Rust 代码，其中包含了函数的属性、清空原始函数的文档注释、添加了一个新的文档注释（包含 OpenSSL 文档的链接），以及函数体。
   - 输出结果：最后，宏将生成的新代码包装为 `TokenStream` 并返回，以便在 Rust 程序中使用。

5. 效果：通过应用 `corresponds` 属性宏，您可以自动生成包含文档链接的注释，以明确指出 Rust 函数与 OpenSSL 文档中的函数的对应关系。这有助于使代码更易于理解，并提供了方便的文档导航功能。

总之，`corresponds` 属性宏是一个在 Rust 中自动生成文档链接注释的有用工具，它增强了代码文档的质量和可读性，特别是在与外部库或工具的集成中。


## SerialNumber 的依赖链条


```
pub struct Asn1Integer;
/// A reference to an [`Asn1Integer`].
pub struct Asn1IntegerRef;
```









## SerialNumber 的使用场景


```Rust
impl X509Ref {
    /// Sets the serial number of the certificate.
    #[corresponds(X509_set_serialNumber)]
    pub fn set_serial_number(&mut self, serial_number: &Asn1IntegerRef) -> Result<(), ErrorStack> {
        unsafe {
            cvt(ffi::X509_set_serialNumber(
                self.0.as_ptr(),
                serial_number.as_ptr(),
            ))
            .map(|_| ())
        }
    }


    /// Returns this certificate's serial number.
    #[corresponds(X509_get_serialNumber)]
    pub fn serial_number(&self) -> &Asn1IntegerRef {
        unsafe {
            let r = ffi::X509_get_serialNumber(self.as_ptr());
            Asn1IntegerRef::from_const_ptr_opt(r).expect("serial number must not be null")
        }
    }
}

impl Ord for X509Ref {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        // X509_cmp returns a number <0 for less than, 0 for equal and >0 for greater than.
        // It can't fail if both pointers are valid, which we know is true.
        let cmp = unsafe { ffi::X509_cmp(self.as_ptr(), other.as_ptr()) };
        cmp.cmp(&0)
    }
}













impl X509RevokedRef {
    /// Get the serial number of the revoked certificate
    #[corresponds(X509_REVOKED_get0_serialNumber)]
    pub fn serial_number(&self) -> &Asn1IntegerRef {
        unsafe {
            let r = X509_REVOKED_get0_serialNumber(self.as_ptr() as *const _);
            assert!(!r.is_null());
            Asn1IntegerRef::from_ptr(r as *mut _)
        }
    }

    /// Get the revocation status of a certificate by its serial number
    #[corresponds(X509_CRL_get0_by_serial)]
    pub fn get_by_serial<'a>(&'a self, serial: &Asn1IntegerRef) -> CrlStatus<'a> {
        unsafe {
            let mut ret = ptr::null_mut::<ffi::X509_REVOKED>();
            let status =
                ffi::X509_CRL_get0_by_serial(self.as_ptr(), &mut ret as *mut _, serial.as_ptr());
            CrlStatus::from_ffi_status(status, ret)
        }
    }
}
```



## X509 rust-openssl/openssl/src/cms.rs
















 











