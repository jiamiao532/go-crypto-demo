#include <iostream>
#include <vector>
#include <string>

int main() {
    std::vector<char> charVector = { char(1), char(68) };

    // 解释 vector<char> 为整数值
    int integerValue = 0;
    for (char c : charVector) {
        integerValue = (integerValue << 8) | static_cast<unsigned char>(c);
    }

    // 将整数值转换为十进制字符串
    std::string decimalString = std::to_string(integerValue);

    std::cout << "Decimal string: " << decimalString << std::endl;

    return 0;
}
