package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	// 生成 RSA 私钥
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		fmt.Println("生成私钥失败:", err)
		return
	}

	// 构造证书请求 (CSR)
	template := x509.CertificateRequest{
		Subject: pkix.Name{
			CommonName:   "example.com",
			Organization: []string{"Example Organization"},
		},
		SignatureAlgorithm: x509.SHA256WithRSA,
	}

	csrDER, err := x509.CreateCertificateRequest(rand.Reader, &template, privateKey)
	if err != nil {
		fmt.Println("创建证书请求失败:", err)
		return
	}

	caURL := "http://localhost:8080/submit"
	resp, err := http.Post(caURL, "application/x-pem-file", bytes.NewBuffer(csrDER))
	if err != nil {
		fmt.Println("提交证书请求失败:", err)
		return
	}
	defer resp.Body.Close()

	certDER, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("读取证书失败:", err)
		return
	}

	// 保存证书到文件
	fmt.Println(string(certDER))

	fmt.Println("证书签发成功")
}
