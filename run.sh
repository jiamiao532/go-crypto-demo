pid=$(lsof -i:8080 | grep ^server | awk '{print $2}')
# 如何 pid不为空 kill -9 ${pid}
if [ -n "$pid" ]; then
    kill -9 ${pid}
fi

rm -f log.stdout log.stderr
# rm -f *.pem
go run server.go > log.stdout 2> log.stderr &
if [ $? -ne 0 ]; then
    echo "Failed to start server"
    exit 1
fi