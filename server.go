package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	mathRand "math/rand"
	"net/http"
	"os"
	"time"
)

func main() {
	rolesConfig := map[string]int64{
		"famer":   3,
		"hunter":  1,
		"witch":   1,
		"prophet": 1,
		"wolf":    3,
	}

	rolesPool := make([]string, 0)
	for role, count := range rolesConfig {
		for i := 0; i < int(count); i++ {
			rolesPool = append(rolesPool, role)
		}
	}

	caCert, err := ioutil.ReadFile("ca_cert.pem") // 替换为你的 CA 证书路径
	if err != nil {
		log.Fatal("Read ca certificate: ", err)
	}

	caKey, err := ioutil.ReadFile("ca_private_key.pem") // 替换为你的 CA 私钥路径
	if err != nil {
		log.Fatal("Read ca private key: ", err)
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// 读取客户端提交的 CSR Certificate sign request
		csrData, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "Read csr failed", http.StatusBadRequest)
			return
		}

		// 验证 CSR 并签发证书
		// 这部分需要自己的逻辑，用于验证 CSR 和签发数字证书的过程
		fmt.Println(caKey)
		fmt.Println(csrData)
		certData := []byte("hello")

		// response
		w.Header().Set("Content-Type", "application/x-x509-user-cert")
		w.Write(certData)
	})

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hi"))
		w.WriteHeader(http.StatusOK)
	})

	http.HandleFunc("/select_role", func(w http.ResponseWriter, r *http.Request) {
		// random select a role from pool
		mathRand.Seed(time.Now().UnixNano())
		roleIndex := mathRand.Intn(len(rolesPool))
		role := rolesPool[roleIndex]
		w.Write([]byte(role))
		w.WriteHeader(http.StatusOK)
	})

	http.HandleFunc("/submit", func(w http.ResponseWriter, r *http.Request) {
		clientAddr := r.RemoteAddr
		INFO("client ip: ", clientAddr)

		// 读取客户端提交的 CSR Certificate sign request
		csrData, err := ioutil.ReadAll(r.Body)
		if err != nil {
			ERROR("Read csr failed: ", err)
			http.Error(w, "Read csr failed", http.StatusInternalServerError)
			return
		}

		// 解析 CSR 数据
		csr, err := x509.ParseCertificateRequest(csrData)
		if err != nil {
			ERROR("Parse csr failed: ", err)
			http.Error(w, "Parse csr failed", http.StatusBadRequest)
			return
		}

		// 验证 CSR 有效性
		if err := csr.CheckSignature(); err != nil {
			ERROR("CSR signature failed: ", err)
			http.Error(w, "CSR signature failed", http.StatusBadRequest)
			return
		}

		// 创建一个自签名的根证书
		rootKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
		if err != nil {
			ERROR("Generate root key failed: ", err)
			http.Error(w, "Generate root key failed", http.StatusInternalServerError)
			return
		}

		template := x509.Certificate{
			SerialNumber:          big.NewInt(1),
			Subject:               pkix.Name{Organization: []string{"Example CA"}},
			NotBefore:             time.Now(),
			NotAfter:              time.Now().Add(365 * 24 * time.Hour),
			KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageDigitalSignature,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
			BasicConstraintsValid: true,
			IsCA:                  true,
		}

		rootCertDER, err := x509.CreateCertificate(rand.Reader, &template, &template, &rootKey.PublicKey, rootKey)
		if err != nil {
			ERROR("Create root certificate failed: ", err)
			http.Error(w, "Create root certificate failed", http.StatusInternalServerError)
			return
		}

		rootCertPEM := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: rootCertDER})

		INFO("证书签发成功")

		w.Header().Set("Content-Type", "application/x-pem-file")
		w.WriteHeader(http.StatusOK)
		w.Write(rootCertPEM)

	})

	// server := &http.Server{
	// 	Addr: ":8080",
	// }

	INFO("CA Server start, listen port 8080...")
	err = http.ListenAndServe(":8080", nil)
	// err = server.Li("ca_cert.pem", "ca_private_key.pem")
	if err != nil {
		ERROR("Server start failed: ", err)
		os.Exit(1)
	}

}

func INFO(message string, args ...interface{}) {
	currentTime := time.Now().Format("2006-01-02 15:04:05")
	formattedMessage := fmt.Sprintf(message, args...)
	fmt.Fprintf(os.Stdout, "[INFO] %s %s\n", currentTime, formattedMessage)
}

func ERROR(message string, args ...interface{}) {
	currentTime := time.Now().Format("2006-01-02 15:04:05")
	formattedMessage := fmt.Sprintf(message, args...)
	fmt.Fprintf(os.Stderr, "[Error] %s %s\n", currentTime, formattedMessage)
}
